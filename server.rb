# -*- encoding: utf-8 -*-
require 'sinatra'

FILEPATH="/tmp/messages"

get '/hello' do
  unless params[:language].nil?
    hellos = {"spain" => "Hola, mundo!",
              "thai" => "สวัสดีชาวโลก!",
              "japan" => "こんにちは 世界!",
              "german" => "hallo Welt!",
              "english" => "Hello, World!"}
    hellos[params[:language]]
  else
    hellos = ["Hello World", "こんにちは 世界", "여보세요 세계", "Zdravo, svijete!", "Zdravo, svet!", "Sveikas, pasauli!", "Hola, mundo!"]
    hellos[rand(hellos.length)] + "\n"
  end
end

post '/message' do
  "Thanks meesage.:-)\n\n" + params[:message]
end

get '/messages' do
  open("#{FILEPATH}/#{params[:title]}").read
end

get '/messages/list' do
  `ls #{FILEPATH}` + "\n"
end

post '/messages' do
  File.write("#{FILEPATH}/#{params[:title]}", params[:message])
  "post success!!\n"
end

put '/messages' do
  File.write("#{FILEPATH}/#{params[:title]}", params[:message])
  "edit success!!\n"
end

delete '/messages' do
  File.delete("#{FILEPATH}/#{params[:title]}")
  "delete success!!\n"
end
